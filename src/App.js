import "./App.css";
import Background from "./Background/Background";
import Body from "./Body/Body";
import Header from "./Header/Header";

function App() {
  return (
    <div className="App">
      <Background />
      <div className=" relative z-10">
        <Header />
        <Body />
      </div>
    </div>
  );
}

export default App;
