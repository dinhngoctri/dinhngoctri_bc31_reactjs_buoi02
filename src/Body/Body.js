import React, { Component } from "react";
import { virtualGlassesData } from "./bodyData/virtualGlassesData";
import GlassesList from "./GlassesList/GlassesList";
import Models from "./Models/Models";

export default class Body extends Component {
  state = {
    data: virtualGlassesData,
  };
  handleChangeVirtualGlasses = (index) => {
    this.setState({
      data: virtualGlassesData[index],
    });
  };
  render() {
    return (
      <div className="container mx-auto px-20">
        <div className="my-12 px-20 flex justify-between">
          <Models item={virtualGlassesData[6]} />
          <Models item={this.state.data} />
        </div>
        <GlassesList changeGlassesFunction={this.handleChangeVirtualGlasses} />
      </div>
    );
  }
}
