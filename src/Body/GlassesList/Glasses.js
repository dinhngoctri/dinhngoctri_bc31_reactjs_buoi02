import React, { Component } from "react";

export default class Glasses extends Component {
  render() {
    return (
      <div
        className={`w-full h-16 border-2 bg-[url('./bai-tap-glasses/glassesImage/${this.props.img}')] bg-contain bg-center bg-no-repeat cursor-pointer`}
        onClick={() => {
          this.props.changeGlassesFunction(this.props.index);
        }}
      ></div>
    );
  }
}
