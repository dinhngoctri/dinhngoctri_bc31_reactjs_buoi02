import React, { Component } from "react";
import { glassesData } from "../bodyData/glassesData";
import Glasses from "./Glasses";

export default class GlassesList extends Component {
  state = {
    glasseseData: glassesData,
  };
  renderGlassesList = () => {
    return this.state.glasseseData.map((item, index) => {
      return (
        <Glasses
          key={(index * 22) / 7}
          img={item}
          index={index}
          changeGlassesFunction={this.props.changeGlassesFunction}
        />
      );
    });
  };

  render() {
    return (
      <div className="grid grid-cols-6 gap-4 p-6 bg-white">
        {this.renderGlassesList()}
      </div>
    );
  }
}
