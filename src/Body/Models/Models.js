import React, { Component } from "react";
import VirtualGlassesAndInfo from "./VirtualGlassesAndInfo";

export default class Models extends Component {
  render() {
    return (
      <div
        className="w-80 h-96 relative bg-[url('./bai-tap-glasses/glassesImage/model.jpg')] bg-contain bg-center bg-no-repeat"
        style={{ backgroundColor: "#fbfcfc" }}
      >
        {this.props.item.name != undefined && (
          <VirtualGlassesAndInfo item={this.props.item} />
        )}
      </div>
    );
  }
}
