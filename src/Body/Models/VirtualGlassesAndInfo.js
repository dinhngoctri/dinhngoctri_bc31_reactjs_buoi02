import React, { Component } from "react";

export default class VirtualGlassesAndInfo extends Component {
  render() {
    return (
      <div>
        <div
          className="absolute opacity-90"
          style={{ width: "162px", top: "26%", left: "25%" }}
        >
          <img src={`./bai-tap-glasses${this.props.item.url}`} alt="..." />
        </div>
        <div className="absolute bottom-0 p-2 bg-orange-300 text-left">
          <h3 className="mb-2 text-2xl text-blue-500">
            {this.props.item.name}
          </h3>
          <p>{this.props.item.desc}</p>
        </div>
      </div>
    );
  }
}
