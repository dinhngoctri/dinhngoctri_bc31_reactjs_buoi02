import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div className="py-8 bg-current opacity-50">
        <h3 className="text-3xl text-white">Try Glasses App Online</h3>
      </div>
    );
  }
}
